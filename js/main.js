
(function ($) {
    "use strict";
    
    
    /*==================================================================
    [ Focus input ]*/
    $('.input100').each(function(){
        $(this).on('blur', function(){
            if($(this).val().trim() != "") {
                $(this).addClass('has-val');
            }
            else {
                $(this).removeClass('has-val');
            }
        })    
    })
  
  
    /*==================================================================
    [ Validate ]*/
    var input = $('.validate-input .input100');
/*
    $('.validate-form').on('submit',function(){
        var check = true;

        for(var i=0; i<input.length; i++) {
            if(validate(input[i]) == false){
                showValidate(input[i]);
                check=false;
            }
        }

        return check;
    });*/


    $('.validate-form .input100').each(function(){
        $(this).focus(function(){
           hideValidate(this);
        });
    });

    function validate (input) {
        if($(input).attr('type') == 'email' || $(input).attr('name') == 'email') {
            if($(input).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
              
                return false;
            }
        }
        else {
            if($(input).val().trim() == ''){
                return false;
            }
        }
    }

    function showValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).addClass('alert-validate');
    }

    function hideValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).removeClass('alert-validate');
    }
    
    /*==================================================================
    [ Show pass ]*/
    var showPass = 0;
    $('.btn-show-pass').on('click', function(){
        if(showPass == 0) {
            $(this).next('input').attr('type','text');
            $(this).addClass('active');
            showPass = 1;
        }
        else {
            $(this).next('input').attr('type','password');
            $(this).removeClass('active');
            showPass = 0;
        }
        
    });
    
    // Added for team polling on window load


    var teams = {};
    var select = document.getElementById("mySelect");
 
    $.ajax({
		url : "http://sgpoc-ssp.apps.42dd.example.opentlc.com/model/teams",
		type : "GET",
		contentType : "application/json",
		dataType : "json",
        success : function(response) {
            if (response == undefined) {
				alert("Data Retrieval Error!");
				return;
			} else {
				
                teams = response;
                sessionStorage.setItem("teamsJson",JSON.stringify(response));
                for (var item in teams.data) {
                    console.log(teams.data[item].attributes.name);
                    var option = document.createElement("OPTION"),
                        txt = document.createTextNode(teams.data[item].attributes.name);
                    
                    option.className = "dropdownlook";
                    option.appendChild(txt);
                    select.insertBefore(option,select.lastChild);
                }
			}
            
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			alert("Login Error: " + errorThrown + "\nPlease log again");
             window.open("http://dvtmsantawebtest-ssp.apps.42dd.example.opentlc.com", "_self");
		}
    });
    
    

    
    
    
    
    


})(jQuery);
