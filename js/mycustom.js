var myEmail = document.getElementById("myEmailInput") 
var myPassword = document.getElementById("myPasswordInput");





function mycustomlogin(){
    var $form = $("#loginform");
	var data = getFormData($form);
	var s = JSON.stringify(data);
    console.log(s);
    
    
    
    console.log("Login triggered!");
	$.ajax({
		url : "http://sgpoc-ssp.apps.42dd.example.opentlc.com/processes/login",
		type : "POST",
		data : s,
		contentType : "application/json",
		dataType : "json",
        success : function(user) {
            if (user == undefined) {
				alert("User doesn't exist!");
				return;
			} else {
				sessionStorage.setItem("id", user.token.id);
                sessionStorage.setItem("key", user.token.key)
				console.log("Logged user saved in localStorage!");
                
                var myId = sessionStorage.getItem('id');
                var myKey = sessionStorage.getItem('key')
                
                 $.ajax({
                        url : "http://sgpoc-ssp.apps.42dd.example.opentlc.com/model/users/"+myId,
                        type : "GET",
                        headers : {'Authorization': myId+"|"+myKey},
                        contentType : "application/json",
                        dataType : "json",
                        success : function(response) {
                            if (response == undefined) {
                                alert("Data Retrieval Error!");
                                return;
                            } else {
                                sessionStorage.setItem("attemail", response.data.attributes.email);
                                sessionStorage.setItem("attpassword", response.data.attributes.password)
                                sessionStorage.setItem("attcurrency", response.data.attributes.currency)
                                sessionStorage.setItem("attinterests", response.data.attributes.interests)
                                sessionStorage.setItem("attuser_price_cap", response.data.attributes.user_price_cap)
                                sessionStorage.setItem("attuser_delivery_day", response.data.attributes.user_delivery_day)

                                sessionStorage.setItem("ssid", response.data.relationships.ssid.data.id);
                                sessionStorage.setItem("teamid", response.data.relationships.team_id.data.id);
                                
                                if( response.data.relationships.ssid.data.id!== null && response.data.relationships.ssid.data.id !== '') {
                                window.open("http://dvtmsantawebtest-ssp.apps.42dd.example.opentlc.com/home.html","_self")
                                }
                                else {
                                window.open("http://dvtmsantawebtest-ssp.apps.42dd.example.opentlc.com/start.html","_self")
                                }
                            }

                        },
                        error : function(XMLHttpRequest, textStatus, errorThrown) {
                            alert("Login Error: " + errorThrown + "\nPlease log again!");
                             window.open("http://dvtmsantawebtest-ssp.apps.42dd.example.opentlc.com", "_self");
                        }
                    });
                
             
			}
        
            
            
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			alert("Login Error: " + errorThrown);
		}
        
        
        
        
        
        
	   
	});
    
  
    
    
    function getFormData($form) {
	var unindexed_array = $form.serializeArray();
	var indexed_array = {};

	$.map(unindexed_array, function(n, i) {
		indexed_array[n['name']] = n['value'];
	});

	return indexed_array;
}
    
    
}



function mycustomsignup(){
    var $form = $("#signupform");
	var data = getFormData($form);
	
    var selectedIndex = document.getElementById("mySelect").selectedIndex;
    var teamJson = JSON.parse(sessionStorage.getItem("teamsJson"));
    
    var uploadAtt = {};
        uploadAtt['email']=data.email;
        uploadAtt['interests']=data.interests;
    
    var uploadTeamId = {};
        uploadTeamId['id']=teamJson.data[selectedIndex].id;
    
    var uploadTeamData= {};
    
        uploadTeamData['data']=uploadTeamId;
    
    var uploadTeam = {};
        uploadTeam['team_id']=uploadTeamData;
    
    var upload = {};
        upload['attributes']=uploadAtt;
        upload['relationships']=uploadTeam;
    
    var wrapped = JSON.stringify(upload);
    
    if (data.email.trim().endsWith("@devoteam.com") ) 
        {
            $.ajax({
		url : "http://sgpoc-ssp.apps.42dd.example.opentlc.com/model/users",
		type : "POST",
		data : wrapped,
		contentType : "application/json",
		dataType : "json",
        success : function(user) {
            if (user == undefined) {
				alert("User doesn't exist!");
				return;
			} else {
				alert("Sign up successful!");
                window.open("http://dvtmsantawebtest-ssp.apps.42dd.example.opentlc.com", "_self")
			}
            
            
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			alert("Sign Up Error: " + errorThrown);
		}
        
	   
	}); 
        }
    
    else {
        alert("You must use a Devoteam Email!");
    }
  
    
    
    function getFormData($form) {
	var unindexed_array = $form.serializeArray();
	var indexed_array = {};

	$.map(unindexed_array, function(n, i) {
		indexed_array[n['name']] = n['value'];
	});

	return indexed_array;
}
    
    
}



function switchToSignUp() {
    
     document.getElementById("myloginform").className = "wrap-login100 displaynone";
    
     document.getElementById("mysignupform").className = "wrap-login100";
}

function switchToLogin() {
    
     document.getElementById("myloginform").className = "wrap-login100";
     document.getElementById("mysignupform").className = "wrap-login100 displaynone";
    
}
